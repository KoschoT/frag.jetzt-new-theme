export const blue = {

  '--primary' : '#370d00',
  '--primary-variant': '#d5d2c1',

  '--secondary': '#370d00',
  '--secondary-variant': '#370d00',

  '--background': '#bd8e62',
  '--surface': '#d5d2c1',
  '--dialog': '#bd8e62',
  '--cancel': '#26343c',
  '--alt-surface': '#323232',
  '--alt-dialog': '#455a64',

  '--on-primary': '#f5e2d4',
  '--on-secondary': '#f5e2d4',
  '--on-background': '#FFFFFF',
  '--on-surface': '#370d00',
  '--on-cancel': '#FFFFFF',

  '--green': 'lightgreen',
  '--red': 'red',
  '--yellow': 'yellow',
  '--blue': '#3f51b5',
  '--purple': '#9c27b0',
  '--light-green': '#80ba24',
  '--grey': '#BDBDBD',
  '--grey-light': '#9E9E9E',
  '--black': '#212121',
  '--moderator': '#fff3e0'
};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'Muted Colors',
      'de': 'Gedeckte Farben'
    },
    'description': {
      'en': 'Muted colors for pleasent reading',
      'de': 'Gedeckte Farben für angenehmes Lesen'
    }
  },
  'isDark': true,
  'order': 3,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'background'

};

